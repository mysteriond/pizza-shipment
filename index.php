<?php

// Vendor includes
require_once __DIR__ . '/vendor/autoload.php';

// Create The Application
$app = new App\System\Application(__DIR__ . '/app');

// Run The Application
$app->run();
