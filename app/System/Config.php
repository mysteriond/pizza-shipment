<?php

namespace App\System;

class Config
{
    /**
     * Config params
     *
     * @var array
     */
    private $params = [];

    /**
     * Create new Config instance
     *
     * @param array $configData
     */
    public function __construct(array $configData)
    {
        $this->params = $configData;
    }

    /**
     * Get config param value
     *
     * @param string $name
     * @param mixed $default
     * @return mixed
     */
    public function get(string $name, $default = null)
    {
        if (array_key_exists($name, $this->params)) {
            return $this->params[$name];
        }
        return $default;
    }

    /**
     * Set config param
     *
     * @param string $name
     * @param mixed $value
     * @return void
     */
    public function set(string $name, $value)
    {
        $this->params[$name] = $value;
    }
}
