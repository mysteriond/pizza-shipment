<?php

namespace App\System;

use Illuminate\Container\Container;
use App\Provider;
use App\Handler;

class Application extends Container
{
    /**
     * The base path of the application installation.
     *
     * @var string
     */
    protected $basePath;

    /**
     * Application's config
     *
     * @var Config
     */
    protected $config;

    /**
     * Create a new application instance.
     *
     * @return void
     */
    public function __construct(string $basePath)
    {
        $this->basePath = $basePath;
        
        static::setInstance($this);
        $this->instance('app', $this);
        $this->instance('App\System\Application', $this);

        $this->loadConfig();
        $this->loadHelpers();
    }

    /**
     * Get application's config
     *
     * @return Cofnig
     */
    public function config()
    {
        return $this->config;
    }

    /**
     * Run application
     *
     * @return void
     */
    public function run()
    {
        $this->make(Provider::class)->register();

        return $this->make(Handler::class)->handle();
    }

    /**
     * Register helpers functions
     * 
     * @return void
     */
    protected function loadHelpers()
    {
        require_once $this->basePath . '/Helpers/functions.php';
    }

    /**
     * Load application config
     * 
     * @return void
     */
    protected function loadConfig()
    {
        $this->config = new Config(
            require("{$this->basePath}/Config/config.php")
        );
    }
}
