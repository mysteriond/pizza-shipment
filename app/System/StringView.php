<?php

namespace App\System;

interface StringView
{
    /**
     * Convert object to string
     * 
     * @return string
     */
    public function __toString() : string;
}
