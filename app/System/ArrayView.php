<?php

namespace App\System;

interface ArrayView
{
    /**
     * Convert object to array
     * 
     * @return string
     */
    public function toArray() : array;
}
