<?php

namespace App\Entity;

use App\System\Entity;

class MapPoint extends Entity
{
    /**
     * Point latitude
     *
     * @var int
     */
    private $latitude = 0;

    /**
     * Point longitude
     * 
     * @var int
     */
    private $longitude = 0;

    /**
     * Create new MapPoint instacne
     * 
     * @param int $latitude
     * @param int $longitude
     * @return void
     */
    public function __construct(int $latitude = 0, int $longitude = 0)
    {
        $this->latitude = $latitude;
        $this->longitude = $longitude;
    }

    /**
     * Convert MapPoint to string
     *
     * @return string
     */
    public function __toString() : string
    {
        return (string) $this->latitude . ', ' . (string) $this->longitude;
    }

    /**
     * Convert MapPoint to array
     *
     * @return array
     */
    public function toArray() : array
    {
        return [
            'latitude' => $this->latitude,
            'longitude' => $this->longitude
        ];
    }

    /**
     * Set Point's latitude
     *
     * @param integer $value
     * @return void
     */
    public function setLatitude(int $value)
    {
        $this->latitude = $value;
    }

    /**
     * Set Point's longitude
     *
     * @param integer $value
     * @return void
     */
    public function setLongitude(int $value)
    {
        $this->longitude = $value;
    }

    /**
     * Get Point's latitude
     *
     * @return int
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Get Point's longitude
     *
     * @return int
     */
    public function getLongitude()
    {
        return $this->longitude;
    }
}
