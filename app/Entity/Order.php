<?php

namespace App\Entity;

use App\System\Entity;

class Order extends Entity
{
    /**
     * Order id
     *
     * @var int|null
     */
    private $id;

    /**
     * Order created time
     * 
     * @var int|null
     */
    private $created;

    /**
     * Cooking time minutes
     * 
     * @var int|null
     */
    private $cooking;

    /**
     * Delivery point
     * 
     * @var MapPoint
     */
    private $point;

    /**
     * Create new Order instance
     *
     * @param int $id
     * @param int $created
     * @param int $cooking
     * @param MapPoint $point
     * @return void
     */
    public function __construct(
        int $id = null,
        int $created = null,
        int $cooking = null,
        MapPoint $point
    ) {
        $this->id = $id;
        $this->created = $created;
        $this->cooking = $cooking;
        $this->point = $point;
    }

    /**
     * Convert MapPoint to string
     *
     * @return string
     */
    public function __toString() : string
    {
        return implode('', [
            $this->id,
            str_repeat(' ', 16 - strlen($this->id)),
            $this->created,
            str_repeat(' ', 20 - strlen($this->created)),
            $this->cooking,
            str_repeat(' ', 16 - strlen($this->cooking)),
            $this->point->getLatitude(),
            str_repeat(' ', 14 - strlen($this->point->getLatitude())),
            $this->point->getLongitude()
        ]);
    }

    /**
     * Convert MapPoint to array
     *
     * @return array
     */
    public function toArray() : array
    {
        return [
            'id' => $this->id,
            'created' => $this->created,
            'cooking' => $this->cooking,
            'point' => $this->point->toArray()
        ];
    }

    /**
     * Set order point
     *
     * @param MapPoint $point
     * @return void
     */
    public function setPoint(MapPoint $point)
    {
        $this->point = $point;
    }

    /**
     * Set order cooking time
     *
     * @param integer $minutes
     * @return void
     */
    public function setCooking(int $minutes)
    {
        $this->cooking = $minutes;
    }
    
    /**
     * Set order created time
     *
     * @param integer $minutes
     * @return void
     */
    public function setCreated(int $minutes)
    {
        $this->created = $minutes;
    }

    /**
     * Set order id
     *
     * @param integer $value
     * @return void
     */
    public function setId(int $value)
    {
        $this->id = $value;
    }

    /**
     * Get order point
     *
     * @return MapPoint|null
     */
    public function getPoint()
    {
        return $this->point;
    }

    /**
     * Get order cooking time
     *
     * @return int|null
     */
    public function getCooking()
    {
        return $this->cooking;
    }

    /**
     * Get order created time
     *
     * @return int|null
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Get order id
     *
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }
}
