<?php

namespace App\Entity;

use App\System\Entity;

class RouteElement extends Entity
{
    /**
     * The order instance
     *
     * @var Order|null
     */
    private $order;

    /**
     * The time when the order will be delivered
     *
     * @var int|null
     */
    private $delivery;

    /**
     * The kitchen queue delay
     * 
     * @var int
     */
    private $waiting;

    /**
     * Create new RouteOrder instance
     *
     * @param Order $order
     * @param int $delivery
     * @param int $waiting
     * @return void
     */
    public function __construct(
        Order $order,
        int $delivery = null,
        int $waiting = 0
    ) {
        $this->order = $order;
        $this->delivery = $delivery;
        $this->waiting = $waiting;
    }

    /**
     * Convert instance to string
     *
     * @return string
     */
    public function __toString() : string
    {
        return implode('', [
            $this->order->getId(),
            str_repeat(' ', 16 - strlen($this->order->getId())),
            $this->delivery,
           
        ]);        
    }

    /**
     * Convert instance to array
     *
     * @return array
     */
    public function toArray() : array
    {
        return [
            'order' => $this->order->toArray(),
            'delivery' => $this->delivery,
            'waiting' => $this->waiting
        ];
    }

    /**
     * Getting route element waiting time
     * 
     * @return int
     */
    public function getWaiting()
    {
        return $this->waiting;
    }

    /**
     * Getting route element delivery time
     * 
     * @return int|null
     */
    public function getDelivery()
    {
        return $this->delivery;
    }

    /**
     * Getting route element order
     * 
     * @return Order
     */
    public function getOrder() : Order
    {
        return $this->order;
    }

    /**
     * Setting route element order
     * 
     * @param Order $order
     * @return void
     */
    public function setOrder(Order $order)
    {
        return $this->order = $order;
    }

    /**
     * Setting route element waiting
     * 
     * @param int $waiting
     * @return void
     */
    public function setWaiting(int $waiting)
    {
        return $this->waiting = $waiting;
    }

    /**
     * Setting route element delivery
     * 
     * @param int $delivery
     * @return void
     */
    public function setDelivery(int $delivery)
    {
        return $this->delivery = $delivery;
    }
}
