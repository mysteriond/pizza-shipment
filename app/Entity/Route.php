<?php

namespace App\Entity;

use App\System\Entity;

class Route extends Entity
{
    const STATUS_READY = 'ready';
    const STATUS_WAITING = 'waiting';

    /**
     * Route id
     * 
     * @var int|null
     */
    private $id;

    /**
     * Route elements
     *
     * @var array
     */
    private $elements = [];

    /**
     * Route status
     *
     * @var string
     */
    private $status;

    /**
     * Create new Route instance
     * 
     * @param int $id
     * @param string $status
     * @param array $elements
     * @return void
     */
    public function __construct(
        int $id = null,
        string $status = self::STATUS_WAITING,
        array $elements = []
    ) {
        $this->id = $id;
        $this->status = $status;
        $this->setElements($elements);
    }

    /**
     * Convert instance to string
     *
     * @return string
     */
    public function __toString() : string
    {
        return implode("\n", $this->elements);
    }

    /**
     * Convert instance to array
     *
     * @return array
     */
    public function toArray() : array
    {
        return [
            'id' => $this->id,
            'status' => $this->status,
            'elements' => array_map(function($item) {
                return $item->toArray();
            }, $this->elements)
        ];
    }

    /**
     * Get Route id
     *
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get Route elements
     *
     * @return array
     */
    public function getElements() : array
    {
        return $this->elements;
    }

    /**
     * Get number of order elements
     *
     * @return int
     */
    public function getElementsNumber() : int
    {
        return count($this->elements);
    }

    /**
     * Get Route status
     *
     * @return string
     */
    public function getStatus() : string
    {
        return $this->status;
    }

    /**
     * Checks if route has "waiting" status
     *
     * @return boolean
     */
    public function isWaiting() : bool
    {
        return $this->status === self::STATUS_WAITING;
    }

    /**
     * Checks if route has "ready" status
     *
     * @return boolean
     */
    public function isReady() : bool
    {
        return $this->status === self::STATUS_READY;
    }

    /**
     * Set Route "waiting" status
     *
     * @return void
     */
    public function setWaiting()
    {
        return $this->status = self::STATUS_WAITING;
    }

    /**
     * Set Route "ready" status
     *
     * @return void
     */
    public function setReady()
    {
        return $this->status = self::STATUS_READY;
    }

    /**
     * Set Route id
     *
     * @param int $id
     * @return void
     */
    public function setId(int $id)
    {
        return $this->id = $id;
    }

    /**
     * Set Route elements
     *
     * @param array $elements
     * @return void
     */
    public function setElements(array $elements)
    {
        $this->elements = [];
        foreach ($elements as $element) {
            $this->appendElement($element);
        }
    }

    /**
     * Append route element
     *
     * @param RouteElement $element
     * @return void
     */
    public function appendElement(RouteElement $element)
    {
        return $this->elements[] = $element;
    }
}
