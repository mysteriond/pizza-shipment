<?php

return [

    /**
     * The total number of orders for generation
     */
    'orders_limit' => 50,


    /**
     * The timer itial time
     */
    'timer_start_time' => 0,


     /*
     * The timer tick value
     */
    'timer_step' => 1,
   
  
    /*
     * The total time limit
     */
    'time_limit' => 100000,


    /**
     * The started interval value of Orders time distribution
     */
    'orders_interval_from' => 1,


    /**
     * The ended interval value of Orders time distribution
     */
    'orders_interval_to' => 30,

  
    /**
     * The started interval of distribution for Order's cooking time 
     */
    'cooking_interval_start' => 10,


    /**
     * The ended interval of distribution for Order's cooking time 
     */
    'cooking_interval_end' => 30,

    /**
     * The limit of orders for together processing
     */
    'kitchen_orders_limit' => 2,


    /*
     * The travel speed on map (points/min)
     */
    'map_travel_speed' => 60,


    /*
     * The map's min coordinate value
     */
    'map_min_coordinate' => -1000,


    /*
     * The map's coordinate value
     */
    'map_max_coordinate' => 1000,


    /**
     * The Route elements max number
     */
    'route_size' => 3,


    /**
     * The time limit for order delivery
     */
    'delivery_limit' => 60
];
