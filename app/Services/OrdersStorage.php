<?php

namespace App\Services;

use App\Entity\Order;

class OrdersStorage
{
    /**
     * Orders instanses
     *
     * @var array
     */
    protected $instanses = [];

    /**
     * Last Order id
     *
     * @var integer
     */
    protected $lastId = 0;

    /**
     * Get last instance id
     *
     * @return int
     */
    public function getLastId() : int
    {
        return $this->lastId;
    }

    /**
     * Order instance existense flag
     *
     * @return bool
     */
    public function exists(Order $order) : bool
    {
        $id = $order->getId();

        return $id && array_key_exists($id, $this->instanses);
    }

    /**
     * Find order by id
     *
     * @param int $id
     * @return Order|null
     */
    public function one(int $id)
    {
        if (array_key_exists($id, $this->instanses)) {
            return $this->instanses[$id];
        }

        return null;
    }

    /**
     * Retrieve all instances
     *
     * @return array
     */
    public function all() : array
    {
        return $this->instanses;
    }

    /**
     * Save Order instance to storage
     *
     * @param Order $order
     * @return void
     */
    public function save(Order $order)
    {
        if ($this->exists($order)) {
            $this->instanses[$order->getId()] = $order;
        } else {
            $this->lastId++;
            $order->setId($this->lastId);
            $this->instanses[$this->lastId] = $order;
        }
    }

    /**
     * Delete Order instance from storage
     *
     * @param Order $order
     * @return bool
     */
    public function delete(Order $order) : bool
    {
        if ($this->exists($order)) {
            unset($this->instanses[$order->get()]);
            return true;
        }

        return false;
    }
}
