<?php

namespace App\Services;

use App\Entity\MapPoint;

class PointsFactory
{
    /**
     * Create MapPoint instance with fake data
     *
     * @return Order
     */
    public function create() : MapPoint
    {
        return app()->makeWith(MapPoint::class, [
            'latitude' => $this->fakeCoordinateValue(),
            'longitude' => $this->fakeCoordinateValue()
        ]);
    }

    /**
     * Get fake coordinate value
     *
     * @return int
     */
    protected function fakeCoordinateValue() : int
    {
        return rand(
            config('map_min_coordinate'),
            config('map_max_coordinate')
        );
    }
}
