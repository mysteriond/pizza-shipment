<?php

namespace App\Services;

class MinutesTimer implements Timer
{
    /**
     * Current time state in minutes
     *
     * @var int
     */
    protected $state = 0;

    /**
     * Time limit
     * 
     * @var int
     */
    protected $limit = 0;

    /**
     * Time step
     * 
     * @var int
     */
    protected $step = 1;

    /**
     * Create new MinutesTimer instance
     */
    public function __construct()
    {
        $this->state = config('timer_start_time');
        $this->limit = config('time_limit');
        $this->step = config('timer_step');
    }

    /**
     * Set timer to next position
     *
     * @return bool
     */
    public function tick() : bool
    {
        $this->state += $this->step;
        
        return $this->limit > 0
            ? $this->state < $this->limit
            : true;
    }

    /**
     * Get current timer value
     *
     * @return int
     */
    public function value() : int
    {
        return $this->state;
    }
}
