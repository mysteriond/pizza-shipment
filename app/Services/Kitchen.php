<?php

namespace App\Services;

use App\Entity\Order;

class Kitchen
{
    /**
     * The orders queue
     *
     * @var array
     */
    protected $queue = [];

    /**
     * Processing order
     *
     * @var array
     */
    protected $processing = [];

    /**
     * The orders processing limit
     *
     * @param Order $order
     * @return void
     */
    protected $ordersLimit = 1;

    /**
     * Timer instance
     * 
     * @var Timer
     */
    protected $timer;

    /**
     * Create new Kitchen instance
     * 
     * @param Timer $timer
     * @return void
     */
    public function __construct(Timer $timer)
    {
        $this->timer = $timer;
        $this->ordersLimit = config('kitchen_orders_limit');
    }

    /**
     * Refresh orders queue
     *
     * @return void
     */
    public function refresh()
    {
        foreach ($this->processing as $index => $item) {       
            if ($this->timer->value() - $item['started'] > $item['cooking']) {
                unset($this->processing[$index]);
            }
        }

        while($this->canProcess() && count($this->queue) > 0) {
            $next = array_shift($this->queue);
            $next['started'] = $this->timer->value();
            $this->processing[] = $next;
        }
    }

    /**
     * Proccess order and getting waiting time
     *
     * @param Order $order
     * @return int
     */
    public function process(Order $order) : int
    {
        $item = [
            'id' => $order->getId(),
            'cooking' => $order->getCooking()
        ];
        $waiting = 0;

        if ($this->canProcess()) {
            $item['started'] = $this->timer->value();
            $this->processing[] = $item;
            
        } else {
            $waiting = $this->getWaiting();
            $this->queue[] = $item;
        }

        return $waiting;
    }

    /**
     * Get current waiting time
     *
     * @return int
     */
    protected function getWaiting() : int
    {
        return array_reduce($this->queue, function($i, $item) {
            return $i += $item['cooking'];
        }, $this->getRelease());
    }

    /**
     * Get the release time of first processing item
     *
     * @return int
     */
    protected function getRelease() : int
    {
        return min(
            array_map(function($item) {
                return $item['started'] + $item['cooking'] - $this->timer->value();
            }, $this->processing)
        );
    }

    /**
     * Checks if kitchen can started process order immediately
     *
     * @return boolean
     */
    protected function canProcess() : bool 
    {
        return count($this->processing) < $this->ordersLimit;
    }
}
