<?php

namespace App\Services;

use App\Entity\Order;

class OrdersManager
{
    /**
     * Next order created time
     * 
     * @var int
     */
    protected $nextTime = 0;

    /**
     * Start time distribution value
     * 
     * @var int
     */
    protected $intervalFrom;

    /**
     * End time distribution value
     * 
     * @var int
     */
    protected $intervalTo;

    /**
     * Orders factory serivce instance
     * 
     * @var OrdersFactory
     */
    protected $factory;

    /**
     * Orders storage instance
     *
     * @var OrdersStorage
     */
    protected $storage;

    /**
     * Timer instance
     *
     * @var Timer
     */
    protected $timer;

    /**
     * Create new OrdersManager instance
     */
    public function __construct(
        OrdersFactory $factory,
        OrdersStorage $storage, 
        Timer $timer
    ) {
        $this->intervalFrom = config('orders_interval_start', 1);
        $this->intervalTo = config('orders_interval_end', 30);
        $this->factory = $factory;
        $this->storage = $storage;
        $this->timer = $timer;
        $this->nextTime = $this->getNextTime();
    }


    /**
     * Checks if Order should been created
     * 
     * @param $currentTime
     * @return bool
     */
    public function shouldCreate() : bool
    {
        return $this->timer->value() >= $this->nextTime;
    }

    /**
     * Create new Order
     * 
     * @return Order
     */
    public function create() : Order
    {
        $order = $this->factory->create();
        $this->storage->save($order);
        $this->nextTime = $this->getNextTime();

        return $order;
    }

    /**
     * Get next order created time
     * 
     * @return int
     */
    protected function getNextTime()
    {
        return $this->timer->value() + rand(
            $this->intervalFrom,
            $this->intervalTo
        );
    }
}
