<?php

namespace App\Services;

use App\Entity\Order;

class OrdersFactory
{
    /**
     * Timer instance
     *
     * @var Timer
     */
    protected $timer;

    /**
     * Points factory instance
     *
     * @var PointsFactory
     */
    protected $points;

    /**
     * Create new OrdersFactory instance
     * 
     * @param Timer $timer
     * @param PointsFactory $points
     * @return void
     */
    public function __construct(Timer $timer, PointsFactory $points) {
        $this->timer = $timer;
        $this->points = $points;
    }
    
    /**
     * Create Order instance with fake data
     *
     * @return Order
     */
    public function create() : Order
    {
        $order = app()->makeWith(Order::class, [
            'created' => $this->timer->value(),
            'cooking' => $this->fakeCookingValue(),
            'point' => $this->points->create()
        ]);

        return $order;
    }

    /**
     * Get order's fake cooking time value
     *
     * @return void
     */
    protected function fakeCookingValue()
    {
        return rand(
            config('cooking_interval_start'),
            config('cooking_interval_end')
        );
    }
}
