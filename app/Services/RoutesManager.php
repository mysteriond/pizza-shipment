<?php

namespace App\Services;

use App\Entity\{Route,RouteElement,Order,MapPoint};

class RoutesManager
{
    /**
     * Routes strogae
     *
     * @var Route
     */
    protected $storage;

    /**
     * The map instance
     * 
     * @var Map
     */
    protected $map;

    /**
     * The home Map point
     * 
     * @var MapPoint
     */
    protected $home;

    /**
     * The Timer instance
     * 
     * @var Timer
     */
    protected $timer;

    /**
     * Delivery time limit
     * 
     * @param int
     */
    protected $deliveryLimit;

    /**
     * Max route elements number
     * 
     * @param int
     */
    protected $routeSize;

    /**
     * Kitchen service
     *
     * @var Kitchen
     */
    protected $kitchen;

    /**
     * Routes shipment queue
     *
     * @var array
     */
    protected $queue = [];

    /**
     * Create RoutesManager instance
     * 
     * @param RoutesStorage $storage
     * @param Map $map
     * @param MapPoint $home
     * @param Kitchen $kitchen
     * @param Timer $timer
     * @return void
     */
    public function __construct(
        RoutesStorage $storage,
        Map $map,
        MapPoint $home,
        Kitchen $kitchen,
        Timer $timer
    ) {
        $this->storage = $storage;
        $this->map = $map;
        $this->home = $home;
        $this->timer = $timer;
        $this->kitchen = $kitchen;
        $this->routeSize = config('route_size');
        $this->deliveryLimit = config('delivery_limit');
        $this->queue = $this->storage->waiting();
        $this->sortQueue();
    }

    /**
     * Append Order to queue of routes
     * 
     * @param Order $order
     * @return void
     */    
    public function appendOrder(Order $order)
    {
        $routeElement = app()->makeWith(RouteElement::class, [
            'order' => $order,
            'waiting' => $this->kitchen->process($order)
        ]);

        if (!$this->tryAppend($routeElement)) {
            $delivery = $this->calculateDelivery($routeElement, $this->home);
            $routeElement->setDelivery($delivery);
            $route = app()->make(Route::class);
            $route->appendElement($routeElement);
            $this->storage->save($route);
            $this->queue[] = $route;
        }
    }

    /**
     * Refresh routes queue
     * 
     * @return void
     */
    public function refresh()
    {
        foreach ($this->queue as $index => $route) {
            if ($this->shouldDispatch($route)) {
                $route->setReady();
                $this->storage->save($route);
                unset($this->queue[$index]);
            }
        }

        $this->kitchen->refresh();
    }

    /**
     * Dispatch all queue routes
     * 
     * @return void
     */
    public function dispatchAll()
    {
        foreach ($this->queue as $index => $route) {
            $route->setReady();
            $this->storage->save($route);
            unset($this->queue[$index]);
        }
    }

    /**
     * Checks if route should been dispatched
     *
     * @param Route $route
     * @return boolean
     */
    protected function shouldDispatch(Route $route) : bool
    {
        if ($route->getElementsNumber() >= $this->routeSize) {
            return true;
        }

        foreach ($route->getElements() as $element) {
            if (
                $element->getDelivery() +
                $this->timer->value() -
                $element->getOrder()->getCreated() >=
                $this->deliveryLimit
            ) {
                return true;
            }
        }
        return false;
    }

    /**
     * Trying append element to exists route
     * 
     * @param RouteElement $element
     * @return bool
     */
    protected function tryAppend(RouteElement $element) : bool
    {
        foreach ($this->queue as $routeIndex => $route) {
            /**
             * First, we getting route current elements and append
             * created order's element
             */
            $elements = $route->getElements();
            $elements[] =  $element;

            /**
             * Then, lets sort elements by distance to home
             */
            usort($elements, function($a, $b) {
                return $this->map->distance($a->getOrder()->getPoint(), $this->home) - 
                    $this->map->distance($b->getOrder()->getPoint(), $this->home);
            });

            /**
             * And finally, we iterate through all unique elements combinations
             * and try to find proper variant
             */
            foreach (array_combinations($elements) as $variant) {
                $delivery = [];
                $canComplect = true;

                foreach ($variant as $elementIndex => $element) {
                    $delivery[$elementIndex] = $this->calculateDelivery(
                        $element,
                        (
                            $elementIndex > 0
                                ? $variant[$elementIndex - 1]->getOrder()->getPoint()
                                : $this->home
                        )
                    );

                    $diff = $delivery[$elementIndex] +
                        $this->timer->value() -
                        $element->getOrder()->getCreated();

                    if ($diff > $this->deliveryLimit) {
                        $canComplect = false;
                        break;
                    }
                }

                if ($canComplect) {
                    foreach ($variant as $elementIndex => $element) {
                        $element->setDelivery($delivery[$elementIndex]);
                    }
                    $route->setElements($variant);

                    if ($this->shouldDispatch($route)) {
                        $route->setReady();
                        unset($this->queue[$routeIndex]);
                    } else {
                        $this->sortQueue();
                    }

                    $this->storage->save($route);
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Getting route element delivery time 
     * 
     * @param RouteElement $element
     * @param MapPoint $start
     * @return int
     */
    protected function calculateDelivery(RouteElement $element, MapPoint $start) : int
    {
        return (
            $element->getWaiting() + 
            $element->getOrder()->getCooking() +            
            $this->map->travelTime($start, $element->getOrder()->getPoint())
        );
    }

    /**
     * Sort routes by number of parts and created time
     *
     * @return void
     */
    protected function sortQueue()
    {
        usort($this->queue, function($a, $b) {
            if ($a->getElementsNumber() === $b->getElementsNumber()) {
                return $b->getCreated() - $a->getCreated();
            }
            return $a->getElementsNumber() - $b->getElementsNumber();
        });
    }
}   
