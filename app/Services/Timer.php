<?php

namespace App\Services;

interface Timer
{
    /**
     * Set timer to next position
     *
     * @return bool
     */
    public function tick() : bool;

    /**
     * Get current timer value
     *
     * @return int
     */
    public function value() : int;
}
