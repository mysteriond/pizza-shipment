<?php

namespace App\Services;

use App\Entity\Route;

class RoutesStorage
{
    /**
     * Routes instanses
     *
     * @var array
     */
    protected $instanses = [];

    /**
     * Last Route id
     *
     * @var integer
     */
    protected $lastId = 0;

    /**
     * Get last instance id
     *
     * @return int
     */
    public function getLastId() : int
    {
        return $this->lastId;
    }

    /**
     * Route instance existense flag
     *
     * @return bool
     */
    public function exists(Route $route) : bool
    {
        $id = $route->getId();

        return $id && array_key_exists($id, $this->instanses);
    }

    /**
     * Find route by id
     *
     * @param int $id
     * @return Route|null
     */
    public function one(int $id)
    {
        if (array_key_exists($id, $this->instanses)) {
            return $this->instanses[$id];
        }

        return null;
    }

    /**
     * Retrieve all instances
     *
     * @return array
     */
    public function all() : array
    {
        return $this->instanses;
    }

    /**
     * Retrieve all instances with "waiting" status
     *
     * @return array
     */
    public function waiting() : array
    {
        return array_filter($this->instanses, function($route) {
            return $route->isWaiting();
        });
    }

    /**
     * Retrieve all instances with "ready" status
     *
     * @return array
     */
    public function ready() : array
    {
        return array_filter($this->instanses, function($route) {
            return $route->isReady();
        });
    }

    /**
     * Save Route instance to storage
     *
     * @param Route $route
     * @return void
     */
    public function save(Route $route)
    {
        if ($this->exists($route)) {
            $this->instanses[$route->getId()] = $route;
        } else {
            $this->lastId++;
            $route->setId($this->lastId);
            $this->instanses[$this->lastId] = $route;
        }
    }

    /**
     * Delete Route instance from storage
     *
     * @param Route $route
     * @return bool
     */
    public function delete(Route $route) : bool
    {
        if ($this->exists($route)) {
            unset($this->instanses[$route->get()]);
            return true;
        }

        return false;
    }
}
