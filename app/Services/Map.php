<?php

namespace App\Services;

use App\Entity\MapPoint;

class Map
{
    /**
     * Travel speed
     */
    protected $speed;

    /**
     * Create new Map instance
     */
    public function __construct()
    {
        $this->speed = config('map_travel_speed');
    }

    /**
     * Get the distance between two MapPoints
     * 
     * @param MapPoint $start
     * @param MapPoint $finish
     * @return int
     */
    public function distance(MapPoint $start, MapPoint $finish) : int
    {
        return (int) round(
            sqrt(
                pow($finish->getLatitude() - $start->getLatitude(), 2) +
                pow($finish->getLongitude() - $start->getLongitude(), 2)
            )
        );
    }

    /**
     * Get the travel time from one MapPoint to another
     * 
     * @param MapPoint $start
     * @param MapPoint $finish
     * @return int
     */
    public function travelTime(MapPoint $start, MapPoint $finish) : int
    {
        return (int) round(
            $this->distance($start, $finish) / $this->speed
        );
    }
}
