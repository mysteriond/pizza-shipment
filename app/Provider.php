<?php

namespace App;

use App\System\Application;

class Provider
{
    /**
     * Create new application provider instance
     * 
     * @param Application $app
     * @return void
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Register application services
     * 
     * @return void
     */
    public function register()
    {
        $this->app->singleton('App\Services\Timer', 'App\Services\MinutesTimer');
        $this->app->singleton('App\Services\OrdersStorage');
        $this->app->singleton('App\Services\RoutesStorage');
    }
}