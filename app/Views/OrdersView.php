<?php

namespace App\Views;

use App\Services\OrdersStorage;

class OrdersView
{
    /**
     * Orders storage instance
     * 
     * @var OrdersStorage $storage
     */
    protected $storage;

    /**
     * Create new orders storage instance
     * 
     * @param OrdersStorage $storage
     * @return void
     */
    public function __construct(OrdersStorage $storage)
    {
        $this->storage = $storage;
    }

    /**
     * Show view content
     * 
     * @return void
     */
    public function show()
    {
        $this->header();
        $this->rows();
    }

    /**
     * Show Orders header
     * 
     * @return void
     */
    protected function header()
    {
        print "Список заказов:\n";
        print str_repeat('-', 80) . "\n";
        print "\033[32m<номер заказа> <время поступления> <время готовки> <координата x> <координата y>\033[0m\n";
    }

    /**
     * Show Orders rows
     * 
     * @return void
     */
    protected function rows()
    {
        foreach ($this->storage->all() as $order) {
            print $order . "\n";
        }
        print str_repeat('-', 80) . "\n\n";
    }
}
