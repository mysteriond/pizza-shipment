<?php

namespace App\Views;

use App\Services\RoutesStorage;

class RoutesView
{
    /**
     * Routes storage instance
     * 
     * @var RoutesStorage $storage
     */
    protected $storage;

    /**
     * Create new routes storage instance
     * 
     * @param RoutesStorage $storage
     * @return void
     */
    public function __construct(RoutesStorage $storage)
    {
        $this->storage = $storage;
    }

    /**
     * Show view content
     * 
     * @return void
     */
    public function show()
    {
        $this->header();
        $this->rows();
    }

    /**
     * Show Routes header
     * 
     * @return void
     */
    protected function header()
    {
        print "Список маршрутов:\n";
        print str_repeat('-', 36) . "\n";
        print "\033[32m<номер заказа> <время доставки>\033[0m\n";
    }

    /**
     * Show Routes rows
     * 
     * @return void
     */
    protected function rows()
    {
        foreach ($this->storage->all() as $route) {
            print str_repeat('-', 30) . "\n";
            print $route . "\n";
            print str_repeat('-', 30) . "\n";
        }
        print str_repeat('-', 36) . "\n";
    }
}
