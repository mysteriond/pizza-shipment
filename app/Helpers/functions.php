<?php

use App\System\Application;

if (!function_exists('app')) {
    function app()
    {
        return Application::getInstance();
    }
}

if (!function_exists('config')) {
    function config(string $name, $default = null)
    {
        return Application::getInstance()->config()->get($name, $default);
    }
}

if (!function_exists('array_combinations')) {
    function array_combinations(array $array) : array
    {
        if (count($array) === 1) {
            return [$array];
        }
        
        $variants = [];
        foreach ($array as $index => $item) {
            $child = $array;
            array_splice($child, $index, 1);
            foreach (array_combinations($child) as $variant) {
                $variants[] = array_merge([$item], $variant);
            }
        }
        
        return $variants;
    }
}
