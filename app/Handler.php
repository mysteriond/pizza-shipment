<?php

namespace App;

use App\Services\{OrdersManager,RoutesManager};
use App\Views\{OrdersView,RoutesView};
use App\Services\Timer;

class Handler
{
    /**
     * Orders manager instance
     * 
     * @var OrdersManager
     */
    protected $orders;

    /**
     * Routes manager instance
     *
     * @var RotesManager
     */
    protected $routes;

    /**
     * Orders view instance
     * 
     * @var OrdersView
     */
    protected $ordersView;

    /**
     * Routes view instance
     *
     * @var RotesView
     */
    protected $routesView;

    /**
     * Timer serivce instance
     * 
     * @var Timer
     */
    protected $timer;

    /**
     * Orders limit
     * 
     * @var int
     */
    protected $ordersLimit;

    /**
     * Orders number
     * 
     * @var int
     */
    protected $ordersNumber = 0;

    /**
     * Create new application handler instance
     * 
     * @param OrdersManager $orders
     * @param OrdersView $ordersView,
     * @param RoutesManager $routes
     * @param RoutesView $routesView
     * @param Timer $timer
     * @return void
     */
    public function __construct(
        OrdersManager $orders,
        OrdersView $ordersView,
        RoutesManager $routes,
        RoutesView $routesView,
        Timer $timer
    ) {
        $this->orders = $orders;
        $this->routes = $routes;
        $this->ordersView = $ordersView;
        $this->routesView = $routesView;
        $this->timer = $timer;
        $this->ordersLimit = config('orders_limit');
    }
    
    /**
     * Application run handler
     * 
     * @return void
     */
    public function handle()
    {
        while ($this->timer->tick()) {

            if ($this->orders->shouldCreate()) {

                $this->routes->appendOrder(
                    $this->orders->create()
                );

                if (++$this->ordersNumber >= $this->ordersLimit) {
                    $this->routes->dispatchAll();
                    break;
                }
            }

            $this->routes->refresh();
        }

        $this->ordersView->show();
        $this->routesView->show();

        exit;  
    }
}